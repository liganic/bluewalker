package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"
	"sync"
	"testing"
	"time"

	"gitlab.com/jtaimisto/bluewalker/hci"
)

func MustCreateAddress(str string, random bool) hci.BtAddress {
	addr, err := hci.BtAddressFromString(str)
	if err != nil {
		panic(err)
	}
	if random {
		addr.Atype = hci.LeRandomAddress
	}
	return addr
}

func TestFormatRandomAddress(t *testing.T) {

	testdata := []struct {
		address  hci.BtAddress
		expected string
	}{
		{
			address:  MustCreateAddress("55:D0:F7:48:79:D1", true),
			expected: "55:d0:f7:48:79:d1,random (resolvable private)",
		},
		{
			address:  MustCreateAddress("30:7D:0F:37:8C:FA", true),
			expected: "30:7d:0f:37:8c:fa,random (non-resolvable private)",
		},
		{
			address:  MustCreateAddress("C8:C6:4B:BD:12:10", true),
			expected: "c8:c6:4b:bd:12:10,random (static)",
		},
	}

	for _, test := range testdata {
		out := formatAddress(test.address)
		if out != test.expected {
			t.Errorf("Expected %s, got %s", test.expected, out)
		}
	}
}

func TestDecodeAdStructure(t *testing.T) {
	testdata := []struct {
		data     hci.AdStructure
		expected string
	}{
		{ // Unknown type
			data:     hci.AdStructure{Typ: hci.AdType(0xFE), Data: []byte{0x00}},
			expected: "Unknown (fe): Data: 0x00",
		},
		{ // empty flags
			data:     hci.AdStructure{Typ: hci.AdFlags, Data: []byte{}},
			expected: "Flags: <no data>",
		},
		{ // some flags
			data:     hci.AdStructure{Typ: hci.AdFlags, Data: []byte{0x05}},
			expected: "Flags: [00000101](LE Limited Discoverable,BR/EDR not supported)",
		},
		{ // no flags set
			data:     hci.AdStructure{Typ: hci.AdFlags, Data: []byte{0x00}},
			expected: "Flags: [00000000]",
		},
		{ // too much flags
			data:     hci.AdStructure{Typ: hci.AdFlags, Data: []byte{0x00, 0x01}},
			expected: "Flags: <invalid> Data: 0x0001",
		},

		{ // Device name
			data:     hci.AdStructure{Typ: hci.AdCompleteLocalName, Data: []byte{'a'}},
			expected: "Complete local name: Name: \"a\"",
		},
		{ // name, no data
			data:     hci.AdStructure{Typ: hci.AdCompleteLocalName, Data: []byte{}},
			expected: "Complete local name: <no data>",
		},
		{ // device address, no data
			data:     hci.AdStructure{Typ: hci.AdDeviceAddress, Data: []byte{}},
			expected: "LE Bluetooth Device Address: <no data>",
		},
		{ // random device address
			data:     hci.AdStructure{Typ: hci.AdDeviceAddress, Data: []byte{0x01, 0xd1, 0x79, 0x48, 0xf7, 0xd0, 0x55}},
			expected: "LE Bluetooth Device Address: 55:d0:f7:48:79:d1,random (resolvable private)",
		},
		{ // public device address
			data:     hci.AdStructure{Typ: hci.AdDeviceAddress, Data: []byte{0x00, 0xad, 0xbd, 0xcf, 0x79, 0xbd, 0x54}},
			expected: "LE Bluetooth Device Address: 54:bd:79:cf:bd:ad",
		},
		{ // short device address
			data:     hci.AdStructure{Typ: hci.AdDeviceAddress, Data: []byte{0x00, 0xad, 0xbd, 0xcf}},
			expected: "LE Bluetooth Device Address: <invalid> Data: 0x00adbdcf",
		},
		{ // vendor specific, valid
			data:     hci.AdStructure{Typ: hci.AdManufacturerSpecific, Data: []byte{0x4c, 0x00, 0xaa, 0xbb}},
			expected: "Manufacturer Specific: Apple, Inc. (0x004c), Data: 0xaabb",
		},
		{ // vendor specific, valid, unknown company
			data:     hci.AdStructure{Typ: hci.AdManufacturerSpecific, Data: []byte{0x00, 0x4c, 0xaa, 0xbb}},
			expected: "Manufacturer Specific: Unknown company ID 0x4c00, Data: 0xaabb",
		},
		{ // vendor specific, No data
			data:     hci.AdStructure{Typ: hci.AdManufacturerSpecific, Data: []byte{0x4c, 0x00}},
			expected: "Manufacturer Specific: Apple, Inc. (0x004c)",
		},
		{ // vendor specific, not enough data
			data:     hci.AdStructure{Typ: hci.AdManufacturerSpecific, Data: []byte{0x4c}},
			expected: "Manufacturer Specific: <invalid> Data: 0x4c",
		},
		{ // vendor specific, no data
			data:     hci.AdStructure{Typ: hci.AdManufacturerSpecific, Data: []byte{}},
			expected: "Manufacturer Specific: <no data>",
		},
		{ // service data, valid
			data:     hci.AdStructure{Typ: hci.AdServiceData, Data: []byte{0x11, 0x22, 0xaa, 0xbb}},
			expected: "Service Data: UUID: 0x2211, Data: 0xaabb",
		},
		{ // service data, valid, just UUID
			data:     hci.AdStructure{Typ: hci.AdServiceData, Data: []byte{0x11, 0x22}},
			expected: "Service Data: UUID: 0x2211",
		},
		{ // service data, no UUID
			data:     hci.AdStructure{Typ: hci.AdServiceData, Data: []byte{0x11}},
			expected: "Service Data: <invalid> Data: 0x11",
		},
		{ // service data, empty
			data:     hci.AdStructure{Typ: hci.AdServiceData, Data: []byte{}},
			expected: "Service Data: <no data>",
		},
		{ // service data, exposure notification
			data:     hci.AdStructure{Typ: hci.AdServiceData, Data: []byte{0x6f, 0xfd, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x22, 0x22, 0x22, 0x22}},
			expected: "Service Data: UUID: 0xfd6f, Exposure Notification\n\t\tProximity Identifier: 0x11111111111111111111111111111111, Encrypted Metadata: 0x22222222",
		},
		{ // service data, invalid exposure notification
			data:     hci.AdStructure{Typ: hci.AdServiceData, Data: []byte{0x6f, 0xfd, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x22}},
			expected: "Service Data: UUID: 0xfd6f, Exposure Notification\n\t\t(invalid data) 0x1111111111111111111111111111111122",
		},
		{ // service data, invalid exposure notification
			data:     hci.AdStructure{Typ: hci.AdServiceData, Data: []byte{0x6f, 0xfd}},
			expected: "Service Data: UUID: 0xfd6f, Exposure Notification\n\t\t(invalid data) 0x",
		},
		{ // 16bit service UUID (complete)
			data:     hci.AdStructure{Typ: hci.AdComplete16BitService, Data: []byte{0xaa, 0xbb, 0xcc, 0xdd}},
			expected: "Complete 16 Bit Service Class UUID: 2 entries: 0xbbaa 0xddcc",
		},
		{ // 16bit service UUID
			data:     hci.AdStructure{Typ: hci.AdMore16BitService, Data: []byte{0xaa, 0xbb}},
			expected: "16 Bit Service Class UUID: 1 entry: 0xbbaa",
		},
		{ // 32bit service UUID (complete)
			data:     hci.AdStructure{Typ: hci.AdComplete32BitService, Data: []byte{0xaa, 0xbb, 0xcc, 0xdd, 0xff, 0xee, 0x11, 0x22}},
			expected: "Complete 32 Bit Service Class UUID: 2 entries: 0xddccbbaa 0x2211eeff",
		},
		{ // 32bit service UUID
			data:     hci.AdStructure{Typ: hci.AdMore32BitService, Data: []byte{0xaa, 0xbb, 0xcc, 0xdd}},
			expected: "32 Bit Service Class UUID: 1 entry: 0xddccbbaa",
		},
		{ // 128bit service UUID (complete)
			data: hci.AdStructure{
				Typ: hci.AdComplete128BitService,
				Data: []byte{0xfc, 0x9d, 0xd0, 0xb3, 0xcb, 0x84, 0xe0, 0x84, 0x06, 0x42, 0xf3, 0xf7, 0xe2, 0xe0, 0xbf, 0xcb,
					0xfc, 0x9d, 0xd0, 0xb3, 0xcb, 0x84, 0xe0, 0x84, 0x06, 0x42, 0xf3, 0xf7, 0xe2, 0xe0, 0xbf, 0xcc,
				},
			},
			expected: "Complete 128 Bit Service Class UUID: 2 entries: cbbfe0e2-f7f3-4206-84e0-84cbb3d09dfc ccbfe0e2-f7f3-4206-84e0-84cbb3d09dfc",
		},
		{ // 128bit service UUID
			data: hci.AdStructure{
				Typ:  hci.AdMore128BitService,
				Data: []byte{0xfc, 0x9d, 0xd0, 0xb3, 0xcb, 0x84, 0xe0, 0x84, 0x06, 0x42, 0xf3, 0xf7, 0xe2, 0xe0, 0xbf, 0xcb},
			},
			expected: "128 Bit Service Class UUID: 1 entry: cbbfe0e2-f7f3-4206-84e0-84cbb3d09dfc",
		},
		{ // service UUID, invalid data
			data:     hci.AdStructure{Typ: hci.AdComplete16BitService, Data: []byte{0xaa, 0xbb, 0xcc}},
			expected: "Complete 16 Bit Service Class UUID: <invalid> Data: 0xaabbcc",
		},
		{ // service UUID, no data
			data:     hci.AdStructure{Typ: hci.AdComplete16BitService, Data: []byte{}},
			expected: "Complete 16 Bit Service Class UUID: <no data>",
		},
	}

	for i, test := range testdata {
		name := fmt.Sprintf("%d-%s", i, test.data.Typ.String())
		t.Run(name, func(t *testing.T) {
			out := decodeAdStructure(&test.data)
			if out != test.expected {
				t.Errorf("Expected \"%s\", got \"%s\"\n", test.expected, out)
			}
		})
	}
}

func TestFileOutput(t *testing.T) {
	dirname, err := ioutil.TempDir("", "test")
	if err != nil {
		t.Fatalf("Unable to create tmp directory: %v", err)
	}
	defer os.RemoveAll(dirname)

	fname := filepath.Join(dirname, "out")
	outdata := "a line\n"

	o, err := outputForFile(fname)
	if err != nil {
		t.Errorf("Unable to create output for %s : %v", fname, err)
	}

	if o.isHumanReadable() {
		t.Error("File output is human readable")
	}

	if e := o.write("a line\n"); e != nil {
		t.Errorf("Can not write to output: %v", e)
	}
	o.Close()

	data, err := ioutil.ReadFile(fname)
	if err != nil {
		t.Errorf("Unable to read file created by output: %v", err)
	}
	if string(data) != outdata {
		t.Errorf("Unexpected contents \"%s\" in output file", string(data))
	}

	fname = filepath.Join(dirname, "out.json")
	o, err = outputForFile(fname)
	if err != nil {
		t.Errorf("Unable to create output for %s : %v", fname, err)
	}
	if e := o.writeAsJSON(struct {
		A int
		B string
	}{A: 1, B: "test"}); e != nil {
		t.Errorf("Unable to write JSON output: %v", e)
	}

	data, err = ioutil.ReadFile(fname)
	if err != nil {
		t.Errorf("Unable to read file created by output: %v", err)
	}
	if string(data) != "{\"A\":1,\"B\":\"test\"}\n" {
		t.Errorf("Unexpected contents \"%s\" in output file", string(data))
	}
}

func readAndCheck(rd io.Reader, expected []byte, t *testing.T) {

	buf := make([]byte, len(expected))
	if l, err := rd.Read(buf); err != nil {
		t.Error("Unable to read data")
	} else if l != len(expected) {
		t.Errorf("Expected %d bytes, but did read only %d", l, len(expected))
	} else {
		if !bytes.Equal(buf, expected) {
			t.Errorf("Expected 0x%s, read 0x%s", expected, buf)
		}
	}
}

func TestListeningSocketOutput(t *testing.T) {

	tmpdir := t.TempDir()
	sockname := filepath.Join(tmpdir, "test.sock")
	o, err := outputForListeningSocket(sockname)
	if err != nil {
		t.Errorf("Unable to create listening socket output: %v", err)
	}

	c1, err := net.Dial("unix", sockname)
	if err != nil {
		t.Errorf("Unable to connect to listening socket: %v", err)
	}
	c2, err := net.Dial("unix", sockname)
	if err != nil {
		t.Errorf("Unable to connect 2nd socket: %v", err)
	}
	defer c1.Close()
	defer c2.Close()

	// give some time for the accept loop to handle incoming connections
	time.Sleep(time.Duration(500 * time.Millisecond))
	var testdata = "Test1\n"
	o.write(testdata)

	var conns = []net.Conn{c1, c2}
	for _, c := range conns {
		readAndCheck(c, []byte(testdata), t)
	}

	// close one of the clients
	c1.Close()
	testdata = "Test2\n"
	o.write(testdata)
	readAndCheck(c2, []byte(testdata), t)
	o.Close()

	if _, err := c2.Write([]byte{0x00}); err == nil {
		t.Error("Expected c2 to be closed after closing output")
	}
}

func TestSocketOutput(t *testing.T) {

	tmpdir := t.TempDir()
	sockname := filepath.Join(tmpdir, "test.sock")

	l, err := net.Listen("unix", sockname)
	if err != nil {
		t.Fatalf("Unable to start listening socket: %v", err)
	}
	defer l.Close()
	guard := sync.WaitGroup{}
	var client_rd io.Reader
	guard.Add(1)
	go func() {
		client, err := l.Accept()
		if err != nil {
			t.Errorf("Error when accepting: %v", err)
		}
		// indicate that client has connected
		client_rd = client
		guard.Done()
	}()
	o, err := outputForSocket(sockname)
	if err != nil {
		t.Fatalf("Unable to create output: %v", err)
	}
	defer o.Close()
	// wait until connected
	guard.Wait()
	if client_rd == nil {
		t.Fatal("Did not get Client")
	}
	testdata := "Test1\n"
	o.write(testdata)
	readAndCheck(client_rd, []byte(testdata), t)
}

func TestSocketOutputError(t *testing.T) {
	tmpdir := t.TempDir()
	name := filepath.Join(tmpdir, "existing.sock")
	if _, err := os.Create(name); err != nil {
		t.Fatalf("Can not create file: %v", err)
	}
	if _, err := outputForListeningSocket(name); err == nil {
		t.Fatal("Was able to create listening socket with existing file name")
	}
	if _, err := outputForSocket(name); err == nil {
		t.Fatal("Was able to connect socket to existing file")
	}
}
